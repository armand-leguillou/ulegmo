def ulegmo(input):
    output=""
    input=input.lower()
    for i in range (len(input)):
#SPACES
        if input[i]==" ":
            output=output+"RZ"
        if input[i]=="/n":
            output=output+"OG"
#ALPHABET
        elif input[i]=="a":
            output=output+"UL"
        elif input[i]=="b":
            output=output+"EG"
        elif input[i]=="c":
            output=output+"MO"
        elif input[i]=="d":
            output=output+"HI"
        elif input[i]=="e":
            output=output+"DA"
        elif input[i]=="f":
            output=output+"XJ"
        elif input[i]=="g":
            output=output+"LK"
        elif input[i]=="h":
            output=output+"SW"
        elif input[i]=="i":
            output=output+"YE"
        elif input[i]=="j":
            output=output+"NB"
        elif input[i]=="k":
            output=output+"TQ"
        elif input[i]=="l":
            output=output+"ON"
        elif input[i]=="m":
            output=output+"PV"
        elif input[i]=="n":
            output=output+"FC"
        elif input[i]=="o":
            output=output+"RS"
        elif input[i]=="p":
            output=output+"UA"
        elif input[i]=="q":
            output=output+"TL"
        elif input[i]=="r":
            output=output+"OI"
        elif input[i]=="s":
            output=output+"UR"
        elif input[i]=="t":
            output=output+"VB"
        elif input[i]=="u":
            output=output+"JL"
        elif input[i]=="v":
            output=output+"LU"
        elif input[i]=="w":
            output=output+"GE"
        elif input[i]=="x":
            output=output+"OM"
        elif input[i]=="y":
            output=output+"IH"
        elif input[i]=="z":
            output=output+"AD"
#NUMBERS
        elif input[i]=="0":
            output=output+"ID"
        elif input[i]=="1":
            output=output+"BO"
        elif input[i]=="2":
            output=output+"MR"
        elif input[i]=="3":
            output=output+"QA"
        elif input[i]=="4":
            output=output+"ET"
        elif input[i]=="5":
            output=output+"UM"
        elif input[i]=="6":
            output=output+"TY"
        elif input[i]=="7":
            output=output+"SO"
        elif input[i]=="8":
            output=output+"RU"
        elif input[i]=="9":
            output=output+"ZA"
#PUNCTUATION
        elif input[i]==".":
            output=output+"TU"
        elif input[i]=="!":
            output=output+"CE"
        elif input[i]=="?":
            output=output+"WI"
        elif input[i]=="...":
            output=output+"LT"
        elif input[i]==",":
            output=output+"GL"
        elif input[i]==";":
            output=output+"VR"
        elif input[i]==":":
            output=output+"FL"
        elif input[i]=="(":
            output=output+"BF"
        elif input[i]==")":
            output=output+"KR"
        elif input[i]=="[":
            output=output+"OL"
        elif input[i]=="]":
            output=output+"IB"
        elif input[i]=="/":
            output=output+"SC"
        elif input[i]=="+":
            output=output+"SP"
        elif input[i]=="-":
            output=output+"TI"
        elif input[i]=="=":
            output=output+"AL"
        elif input[i]=="—":
            output=output+"BL"
        elif input[i]=="_":
            output=output+"NG"
        elif input[i]=="<":
            output=output+"PU"
        elif input[i]==">":
            output=output+"OP"
        elif input[i]=="^":
            output=output+"HA"
        elif input[i]=="«" or input[i]=="“" or input[i]=="‘" or input[i]=="‹":
            output=output+"GI"
        elif input[i]=="»" or input[i]=="”" or input[i]=="’" or input[i]=="›":
          output=output+"UP"
        elif input[i]=="#":
            output=output+"ZK"
        elif input[i]=="@":
            output=output+"NS"
        elif input[i]=="*":
            output=output+"BE"
        elif input[i]=="&":
            output=output+"OR"
            
            
        else:
            output=output+"*"
        
    return output